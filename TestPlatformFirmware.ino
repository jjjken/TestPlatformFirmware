
//FIRMWARE VERSION
#define FIRMWARE_MAJOR 1 //1 for 1520/1580, 2 for 1780 v1.0, 0 for DEVELOPMENT VERSION
#define FIRMWARE_MINOR 18

/*Hardware timers usage:
 * TIMER 0: used by Arduino for micros(), millis(), etc...
 * TIMER 1: used by Arduino servo library
 * TIMER 2: available
*/

/*Interrupts used
 * For Optical RPM sensor:
 * PCICR |= (1<<PCIE2); //Enable Interrupts for PCIE2 Arduino Pins (D0-7)  
 * PCMSK2 |= (1<<PCINT20); // Pin 4, Servo 1
 * For Electrical RPM sensor:
 * attachInterrupt(0, interrupt_counter, CHANGE); //interrupt will occur twice per cycle
*/

//INCLUDES
#include <avr/io.h>
#include <Arduino.h>
#include <Servo.h>
#include "I2C.h"
#include "types.h"
#include "Error.h"
#include "MMA8452Q.h" // Include the MMA8452Q accelerometer library
#include "RPM_SENSOR.h"
#include "RPM_SENSOR_OPTICAL.h"
#include "ADC_24BITS.h" //Measures load cells and ohmmeter
#include "POWER_SENSORS.h" //Measures current, voltage, power
#include "MS4525DO.h" // Measures the pressure from a pitot tube
#include "RC_SIGNALS.h" //Outputs the ESC and servo pwm signals
#include "OneWire.h" //Handles oneWire devices
#include "Protocol.h" //Handles USB commmunication protocol
#include "AverageFilter.h" // Handles average filtering

//DATA STRUCTURES
board_info_t board_info;
firmware_info_t firmware_info;
basic_control_t basic_control;
pro_control_t pro_control;
basic_sensors_t basic_sensors;
pro_sensors_t pro_sensors;
ohmmeter_t ohmmeter;

//AVERAGING FILTERS (also used in Protocol.cpp)
AverageFilter accxFilter;
AverageFilter accyFilter;
AverageFilter acczFilter;
AverageFilter accVibFilter;
AverageFilter leftLoadCellFilter;
AverageFilter rightLoadCellFilter;
AverageFilter thrustLoadCellFilter;
AverageFilter voltageFilter;
AverageFilter currentFilter;
AverageFilter powerFilter;
AverageFilter rpmFilter;
AverageFilter rpmOpticalFilter;
AverageFilter tempProbe1Filter;
AverageFilter tempProbe2Filter;
AverageFilter tempProbe3Filter;
AverageFilter pressurePFilter;
AverageFilter pressureTFilter;

//VARIABLES
int16_t debug[4] = { 0, 0, 0, 0 };
byte pin_states[4] = { 0, 0, 0, 0 }; //ESC, Servo1, Servo2, Servo3
unsigned long last_poll_report_timestamp = 0;
byte is1520;
byte is1580;
byte is1585;

//OBJECTS
MMA8452Q accelerometer; //Accelerometer
ADC_24BITS adc24; //Load cells and precision ohmmeter
RPM_SENSOR rpm; //RPM timer and comparator interfacing to rpm sensor
RPM_SENSOR_OPTICAL rpm_opt; //RPM timer and comparator interfacing to rpm sensor
ISL28022 power_sensor; //Measures current, voltage, power
RC_SIGNALS rc;
OneWire oneWire;
MS4525DO pitot;

//FUNCTION DEFINITIONS
byte get_pcb_version(void); //Returns the hardware version
float calcVibration(float accx, float accy, float accz); //Calculates vibration level

//MAIN SETUP ROUTINE
void setup()
{
	//Setup serial port
	protocol_init();

	//Get board properties
  Serial.print("Initializing...  ");
	board_info.version = get_pcb_version();
	firmware_info.major = FIRMWARE_MAJOR;
	firmware_info.minor = FIRMWARE_MINOR;
  Serial.println();

	//Start I2C bus
	I2c.begin();
	I2c.pullup(1);
	//I2c.scan();
	
	//Initialize system
  if(is1580 || is1585){
    oneWire.init();
    accelerometer.init();
    pitot.init();
  }
	power_sensor.init();
	adc24.init(is1580, is1585);
	rpm.init();
  rpm_opt.init();
	rc.init();

	//The GUI waits for this message before polling data
	Serial.println("Ready");
}

//MAIN PROGRAM LOOP
void loop()
{
	//Since the sensors polling and the serial communication all happens here, 
	//we want to ensure the serial communication is not slowed down by all the tasks
	//and is therefore allowed to run between each task. Tasks should not wait
	//in busy loops, they should be quick and efficient.
	enum tasks { Acc, LoadCells, PowerMonitor, Control, RPM, Temp, Airspeed, QTY };
	static byte task = 0;
	static byte adc0_channel = 0;
	static byte adc1_channel = 0;
	byte done = 0;
	float tempfloat = 0;
	static uint8_t debug_counter = 0;
	switch (task)
	{
	case Acc:
		if (accelerometer.available() && (is1580 || is1585)){
			accelerometer.read();
			accxFilter.newValue(512.0*accelerometer.cx);
			accyFilter.newValue(512.0*accelerometer.cy);
			acczFilter.newValue(512.0*accelerometer.cz);

      //Calculate the vibration level
      accVibFilter.newValue(512.0*calcVibration(accelerometer.cx, accelerometer.cy, accelerometer.cz));
		}
		break;
  case LoadCells:
    if (ohmmeter.status == OHM_READING){
      byte result_state;
      done = adc24.get_ohms(is1585, &(ohmmeter.ohmmeter_reading), &result_state);
      if (done) ohmmeter.status = OHM_DONE_OK + result_state;
    }else{
      if(is1585){
        //All 3 load cells can be sampled with their own ADC chip
        //On 1585, 
        // ADC0, CH0 = Thrust
        // ADC1, CH0 = Left
        // ADC2, CH0 = Right
        // ADC2, CH1 = Ohm
        if(adc24.get_adc(0, 0, &tempfloat)){
           thrustLoadCellFilter.newValue(tempfloat); 
        }
        if(adc24.get_adc(1, 0, &tempfloat)){
           leftLoadCellFilter.newValue(tempfloat); 
        }
        if(adc24.get_adc(2, 0, &tempfloat)){
           rightLoadCellFilter.newValue(tempfloat); 
        }
      }
      if(is1580){
        //All 3 load cells share 2 ADC chips, so we must ping-pong channels
        //On 1580, 
        // ADC0, CH0 = Right
        // ADC0, CH1 = Ohm
        // ADC1, CH0 = Thrust
        // ADC1, CH1 = Left
        if(adc0_channel){
          done = adc24.get_adc(0, 1, &tempfloat); //ohmmeter, we can ignore
        }else{
          done = adc24.get_adc(0, 0, &tempfloat); //right side
          if (done) rightLoadCellFilter.newValue(tempfloat);
        }
        if (done) adc0_channel = 1 - adc0_channel; //toggle channel
        if(adc1_channel){
          done = adc24.get_adc(1, 1, &tempfloat); //left side
          if (done) leftLoadCellFilter.newValue(tempfloat);
        }else{
          done = adc24.get_adc(1, 0, &tempfloat); //thrust
          if (done) thrustLoadCellFilter.newValue(tempfloat);
        }
        if (done) adc1_channel = 1 - adc1_channel; //toggle channel
      }
      if(is1520){
        //Load cell measures only thrust, but we still ping-ping for legacy reasons and because the Series 1580 has to do it too.
        //On 1520, 
        // ADC0, CH0 = Thrust
        // ADC0, CH1 = Empty
        //Thurst shared with 
        if(adc0_channel){
          done = adc24.get_adc(0, 1, &tempfloat); //nothing there
        }else{
          done = adc24.get_adc(0, 0, &tempfloat); //thurst
          if (done) thrustLoadCellFilter.newValue(tempfloat); 
        }
        if (done) adc0_channel = 1 - adc0_channel; //toggle channel
      }
    }
    break;
	case PowerMonitor:
		voltageFilter.newValue(power_sensor.voltage());
		currentFilter.newValue(power_sensor.current());
    powerFilter.newValue(power_sensor.power());
		break;
	case Control:
		if (millis() - last_poll_report_timestamp < 500){
			rc.set(basic_control.ESC_PWM, 0);
			rc.set(pro_control.S1_PWM, 1);
			rc.set(pro_control.S2_PWM, 2);
			rc.set(pro_control.S3_PWM, 3);
		}else{
			rc.init(); //Nothing received from the GUI for a while, disconnect pwm signals.
		}
		break;
	case RPM:
		rpmFilter.newValue(rpm.read_brushless_hz());
        // Disable if servo 1 is active
        if (rc.isActive(1)){
            rpmOpticalFilter.getAverage(); //clear any saved values
            rpmOpticalFilter.newValue(0); //force reading to be zero
        }
    else {
            rpmOpticalFilter.newValue(rpm_opt.read_hz());
    }
		break;
    case Temp: //Temp probes
        if(is1580 || is1585){ // only on Series 15xx
          float t1, t2, t3;
          if(oneWire.readDS18B20(&t1, &t2, &t3)){
            tempProbe1Filter.newValue(t1);
            tempProbe2Filter.newValue(t2);
            tempProbe3Filter.newValue(t3);
          }
        }
    break;
    case Airspeed:
        if (pitot.available() && (is1580 || is1585)){ // Read pressure connected to Pitot
            // Poll the sensor. Ok is _status == 0
            if(!pitot.read()){
                pressurePFilter.newValue(pitot.pressure_P);
                pressureTFilter.newValue(pitot.pressure_T);
            }            
        }
    break;
    default:
		break;
	}
	task = (++task) % QTY;

  //Additional debugging information
  pin_states[0] = digitalRead(3); //0 - ESC - physical pin 1  - PD3 -> 3
  pin_states[1] = digitalRead(4); //1 - S1  - physical pin 2  - PD4 -> 4
  pin_states[2] = digitalRead(5); //2 - S2  - physical pin 9  - PD5 -> 5
  pin_states[3] = digitalRead(8); //3 - S3  - physical pin 12 - PB0 -> 8 

	//Handle serial port between tasks
	serialCom();
}

//Calculates vibration level using simple filters
float calcVibration(float accx, float accy, float accz){
  static float accxlpf = 0;
  static float accylpf = 0;
  static float acczlpf = 0;
  static float vibration = 0;

  static byte init = false;
  if(!init){
    accxlpf = accx;
    accylpf = accy;
    acczlpf = accz;
    init = true;
  }

  //Acc high pass filter
  float accxhpf = accx-accxlpf;
  float accyhpf = accy-accylpf;
  float acczhpf = accz-acczlpf;

  //Instant amplitude deviation
  float amplitude = sqrt(accxhpf*accxhpf + accyhpf*accyhpf + acczhpf*acczhpf);

  //Vibration filter with different decay/gain factors
  #define FILTER 0.9925
  vibration = FILTER * vibration + (1.0-FILTER) * amplitude;
  
  return vibration;
}

//gets the ID from pins
byte getID() {
    // V0, LSB, pin AN1
    // V1, pin AN2
    // V2, pin AN6
    // V3, MSB, AN7
    pinMode(A1, INPUT);
    pinMode(A2, INPUT);
    pinMode(A6, INPUT);
    pinMode(A7, INPUT);
    byte an6 = analogRead(A6)>512;
    byte an7 = analogRead(A7)>512;
    return digitalRead(A1) + (digitalRead(A2) << 1) + (an6 << 2) + (an7 << 3);
}

//Returns the hardware edition/version.
byte get_pcb_version(void){
	//0000 -> series 1580 v1 with 0.003 shunts
  //0001 -> series 1520 v1 with 0.003 shunts
  //0010 -> series 1580 v1 with 0.004 shunts
  //0011 -> series 1780 v1
  //0100 -> series 1585 v1 with 0.004 shunts
	byte version = getID();

  switch(version){
    case 0: // Series 1580v1 with 0.003 shunt
      Serial.print("1580-0.003");
      is1580 = true;
    break;
    case 1: // Series 1520v2
      Serial.print("1520");
      is1520 = true;
    break; 
    case 2: // Series 1580v1 with 0.004 shunt
      Serial.print("1580-0.004");
      is1580 = true;
    break;
    case 3: // Series 1780v1
      Serial.print("1780");
    break;
    case 4: // Series 1585v1 with 0.004 shunt
      Serial.print("1585-0.004");
      is1585 = true;
    break;
    default:
      Serial.print("undetermined board");
  }
  
	return version + 1; //Returns value between 1 and 16
}





