#ifndef POWER_SENSORS_h
#define POWER_SENSORS_h

#include <Arduino.h>
#include "ISL28022.h" // Power monitor IC

class POWER_SENSORS
{
public:	
	POWER_SENSORS(); // Constructor
	void init(void);
	void print(void);
private:

};

#endif
