#ifndef NAU7802_h
#define NAU7802_h

#include <Arduino.h>

#define NAU7802_h_I2C_Address 0x2A
#define NAU7802_h_STABLE_CONV 1 // number of conversions to discart for reading to be stable. Tweak based on sample rate and testing (NOT DOCUMENTED IN DATASHEET!!!!!) unstable at 0, stable at 1. If calibration not done, this will have to be increased.

////////////////////////////////
// NAU7802 Class Declaration //
////////////////////////////////
class NAU7802
{
public:	
  NAU7802(byte adc_num = 0); // Constructor
	byte init();
	byte get_adc_val(float *value, byte channel); //Call this until it returns true. Then value will hold the adc read voltage.
private:

enum NAU7802_REGISTERS {
	PU_CTRL = 0x00,
	CTRL1 = 0x01,
	CTRL2 = 0x02,
	OCAL1_B2 = 0x03,
	OCAL1_B1 = 0x04,
	OCAL1_B0 = 0x05,
	OCAL1_B3 = 0x06,
	GCAL1_B2 = 0x07,
 	GCAL1_B1 = 0x08,
 	GCAL1_B0 = 0x09,
	OCAL2_B2 = 0x0A,
	OCAL2_B1 = 0x0B,
 	OCAL2_B0 = 0x0C,
	GCAL2_B3 = 0x0D,
	GCAL2_B2 = 0x0E,
 	GCAL2_B1 = 0x0F,
	GCAL2_B0 = 0x10,
	I2C_Control = 0x11,
	ADCO_B2 = 0x12,
	ADCO_B1 = 0x13,
	ADCO_B0 = 0x14,
	OTP_B1 = 0x15,
	OTP_B0 = 0x16,
	Device_Revision_Code = 0x1F
};

enum NAU7802_BITS {
	AVDDS = 7,
	OSCS = 6,
	CR = 5,
	CS = 4,
	PUR = 3,
	PUA = 2,
	PUD_ = 1,
	RR = 0,
	CRP = 7,
	VLDO_2 = 5,
	VLDO_1 = 4,
    VLDO_0 = 3,
	GAINS_2 = 2,
	GAINS_1 = 1,
	GAINS_0 = 0,
	CHS = 7,
	CRS_2 = 6,
	CRS_1 = 5,
	CRS_0 = 4,
	CAL_ERR = 3,
	CALS = 2,
	CALMOD_1 = 1,
	CALMOD_0 = 0,
	SPE_ = 5,
	WPD = 4,
	SI = 3,
	BOPGA = 2,
	TS = 1,
	BGPCP = 0
};

byte _adc_num;
byte _channel;
byte _channel_select_step;
byte _adc_val_step;
byte _average_buffer_size;
float _average_buffer;
byte _conversion_settling; // number of conversions left before reading is valid after channel/gain change
byte _calibrate_step;
byte _calibrate_ctrl2_reg;

// rotating median filter
float _rotating_filter[2][3];

byte _gain[2]; //the current gain for each channel
byte _gain_index[2];
byte _new_gain;
float _adc_value;
float _adc_voltage;
void _ADC_select(void);
byte _calibrate(void);
byte _selectChannel(byte newchannel, byte gain, byte force); //CH0=0, CH1=1, Temp=2 
byte _adc_update(float *voltage, float *adc_voltage); //returns true if new value. Voltage is real input voltage, adc_voltage is the voltagexgain read by the adc.
};

#endif
