#ifndef ERROR_h
#define ERROR_h

#include <Arduino.h>
#include "types.h"

void error(byte error_code);
void red_led(byte value);

#endif