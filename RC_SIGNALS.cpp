#include <Arduino.h>
#include "RC_SIGNALS.h"
#include <Servo.h>
#include "types.h"

Servo esc_pwm;
Servo s1_pwm;
Servo s2_pwm;
Servo s3_pwm;

RC_SIGNALS::RC_SIGNALS(){}

void RC_SIGNALS::init(){
	//Set initial values
	basic_control.ESC_PWM = 0;
	pro_control.S1_PWM = 0;
	pro_control.S2_PWM = 0;
	pro_control.S3_PWM = 0;
        
	//Detach all servos
	set(0,0);
  set(0,1);
  set(0,2);
  set(0,3);
}

boolean RC_SIGNALS::isActive(uint8_t index){
  boolean output;
        switch (index){
		case 0:
			output = this->ESCActive;
			break;
		case 1:
			output = this->servo1Active;
			break;
		case 2:
			output = this->servo2Active;
			break;
		case 3:
			output = this->servo3Active;
			break;
        }
  return output;
}

//0 - ESC - physical pin 1  - PD3 -> 3
//1 - S1  - physical pin 2  - PD4 -> 4
//2 - S2  - physical pin 9  - PD5 -> 5
//3 - S3  - physical pin 12 - PB0 -> 8
void RC_SIGNALS::set(uint16_t value, byte index){
	if (value != 0){
		switch (index){
		case 0:
			esc_pwm.write(value);
			esc_pwm.attach(3);
      this->ESCActive = true;
			break;
		case 1:
			s1_pwm.write(value);
			s1_pwm.attach(4);
      this->servo1Active = true;
			break;
		case 2:
			s2_pwm.write(value);
			s2_pwm.attach(5);
      this->servo2Active = true;
			break;
		case 3:
			s3_pwm.write(value);
			s3_pwm.attach(8);
      this->servo3Active = true;
			break;
		}
	}else{
		switch (index){
		case 0:
			esc_pwm.detach();
      this->ESCActive = false;
			break;
		case 1:
			s1_pwm.detach();
      this->servo1Active = false;
      pinMode(4, INPUT_PULLUP);
			break;
		case 2:
			s2_pwm.detach();
      this->servo2Active = false;
      pinMode(5, INPUT_PULLUP);
			break;
		case 3:
			s3_pwm.detach();
      this->servo3Active = false;
      pinMode(8, INPUT_PULLUP);
			break;
		}
	}
}
