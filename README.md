# RCbenchmark.com test tool firmware

**Firmware for the RCbenchmark.com motor test tool**

## Authors

Developped and maintained by Tyto Robotics Inc. This software is written to support the [RCbenchmark.com](http://www.RCbenchmark.com) USB test tool. This test tool was originally a [fork](https://github.com/multiwii/multiwii-firmware) of the open source project Multiwii.

## GUI

The GUI that interfaces with this firmware is found [here](https://chrome.google.com/webstore/detail/rcbenchmarkcom-gui/loaadjknlfcpljcickkiogkbmollildg). For its source code, please visit [here](https://gitlab.com/TytoRobotics/RCbenchmarkGUI).

## How to use

Download the [Arduino IDE](https://www.arduino.cc/en/Main/Software) and open file TestPlatformFirmware.ino. Select Arduino Pro or Pro Mini, ATmega328 (5V, 16Mhz) as the board.

## Issue trackers

For issues related to the GUI raise them [here](https://gitlab.com/TytoRobotics/RCbenchmarkGUI/issues).<br />
For issues related to this firmware, raise them [here](https://gitlab.com/TytoRobotics/TestPlatformFirmware/issues).

## Developers

We accept clean and reasonable patches, submit them!

### Branching structure

To keep things in order, we opt to use the following [branching structure](http://nvie.com/posts/a-successful-git-branching-model/). Please adhere to it if contributing to this project and always target the **"development"** branch for the pull requests.
