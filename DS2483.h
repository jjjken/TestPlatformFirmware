#ifndef DS2483_h
#define DS2483_h

#include <Arduino.h>

// you can exclude DS2483_search by defining that to 0
#ifndef DS2483_SEARCH
#define DS2483_SEARCH 1
#endif

// You can exclude CRC checks altogether by defining this to 0
#ifndef DS2483_CRC
#define DS2483_CRC 1
#endif

// Select the table-lookup method of computing the 8-bit CRC
// by setting this to 1.  The lookup table enlarges code size by
// about 250 bytes.  It does NOT consume RAM (but did in very
// old versions of DS2483).  If you disable this, a slower
// but very compact algorithm is used.
#ifndef DS2483_CRC8_TABLE
#define DS2483_CRC8_TABLE 1
#endif

// You can allow 16-bit CRC checks by defining this to 1
// (Note that DS2483_CRC must also be 1.)
#ifndef DS2483_CRC16
#define DS2483_CRC16 1
#endif

#define FALSE 0
#define TRUE  1

// Platform specific I/O definitions

#if defined(__AVR__)
#define PIN_TO_BASEREG(pin)             (portInputRegister(digitalPinToPort(pin)))
#define PIN_TO_BITMASK(pin)             (digitalPinToBitMask(pin))
#define IO_REG_TYPE byte
#define IO_REG_ASM asm("r30")
#define DIRECT_READ(base, mask)         (((*(base)) & (mask)) ? 1 : 0)
#define DIRECT_MODE_INPUT(base, mask)   ((*((base)+1)) &= ~(mask))
#define DIRECT_MODE_OUTPUT(base, mask)  ((*((base)+1)) |= (mask))
#define DIRECT_WRITE_LOW(base, mask)    ((*((base)+2)) &= ~(mask))
#define DIRECT_WRITE_HIGH(base, mask)   ((*((base)+2)) |= (mask))

#elif defined(__MK20DX128__)
#define PIN_TO_BASEREG(pin)             (portOutputRegister(pin))
#define PIN_TO_BITMASK(pin)             (1)
#define IO_REG_TYPE byte
#define IO_REG_ASM
#define DIRECT_READ(base, mask)         (*((base)+512))
#define DIRECT_MODE_INPUT(base, mask)   (*((base)+640) = 0)
#define DIRECT_MODE_OUTPUT(base, mask)  (*((base)+640) = 1)
#define DIRECT_WRITE_LOW(base, mask)    (*((base)+256) = 1)
#define DIRECT_WRITE_HIGH(base, mask)   (*((base)+128) = 1)

#elif defined(__SAM3X8E__)
// Arduino 1.5.1 may have a bug in delayMicroseconds() on Arduino Due.
// http://arduino.cc/forum/index.php/topic,141030.msg1076268.html#msg1076268
// If you have trouble with DS2483 on Arduino Due, please check the
// status of delayMicroseconds() before reporting a bug in DS2483!
#define PIN_TO_BASEREG(pin)             (&(digitalPinToPort(pin)->PIO_PER))
#define PIN_TO_BITMASK(pin)             (digitalPinToBitMask(pin))
#define IO_REG_TYPE uint32_t
#define IO_REG_ASM
#define DIRECT_READ(base, mask)         (((*((base)+15)) & (mask)) ? 1 : 0)
#define DIRECT_MODE_INPUT(base, mask)   ((*((base)+5)) = (mask))
#define DIRECT_MODE_OUTPUT(base, mask)  ((*((base)+4)) = (mask))
#define DIRECT_WRITE_LOW(base, mask)    ((*((base)+13)) = (mask))
#define DIRECT_WRITE_HIGH(base, mask)   ((*((base)+12)) = (mask))
#ifndef PROGMEM
#define PROGMEM
#endif
#ifndef pgm_read_byte
#define pgm_read_byte(addr) (*(const byte *)(addr))
#endif

#elif defined(__PIC32MX__)
#define PIN_TO_BASEREG(pin)             (portModeRegister(digitalPinToPort(pin)))
#define PIN_TO_BITMASK(pin)             (digitalPinToBitMask(pin))
#define IO_REG_TYPE uint32_t
#define IO_REG_ASM
#define DIRECT_READ(base, mask)         (((*(base+4)) & (mask)) ? 1 : 0)  //PORTX + 0x10
#define DIRECT_MODE_INPUT(base, mask)   ((*(base+2)) = (mask))            //TRISXSET + 0x08
#define DIRECT_MODE_OUTPUT(base, mask)  ((*(base+1)) = (mask))            //TRISXCLR + 0x04
#define DIRECT_WRITE_LOW(base, mask)    ((*(base+8+1)) = (mask))          //LATXCLR  + 0x24
#define DIRECT_WRITE_HIGH(base, mask)   ((*(base+8+2)) = (mask))          //LATXSET + 0x28

#else
#error "Please define I/O register types here"
#endif


class DS2483
{
  private:

//Defines to control the DS2483
#define DS2483_address 0x18
#define DS2483_CMD_BUS_RST 0xB4
#define DS2483_CMD_RST 0xF0
#define DS2483_CMD_SET_READ_PTR 0xE1
#define DS2483_CMD_1W_WRITE_BYTE 0xA5
#define DS2483_CMD_1W_READ_BYTE 0x96
#define DS2483_CMD_1W_WRITE_BIT 0x87
#define DS2483_CMD_TRIPLET 0x78

//DS2483 Registers
#define DS2483_REGISTER_STATUS 0xF0
#define DS2483_REGISTER_READ_DATA 0xE1
#define DS2483_REGISTER_DEVICE_CONFIG 0xC3
#define DS2483_REGISTER_PORT_CONFIG 0xB4

//DS2483 ROM commands (fowarded to DS2483 bus)
#define ROM_READ 0x33
#define ROM_MATCH 0x55
#define ROM_SEARCH 0xF0
#define ROM_SKIP 0xCC
#define ROM_OVERDRIVE_SKIP 0x3C

byte address[8]; //Temp buffer
 
IO_REG_TYPE bitmask;
volatile IO_REG_TYPE *baseReg;

#if DS2483_SEARCH
    // global search state
    byte ROM_NO[8];
    char LastDiscrepancy;
    byte LastDeviceFlag;
#endif

	byte reset_ds2483(void); //Resets the I2C <-> DS2483 bridge

  public:
    DS2483(void);

	//Busy wait until the bus is idle before the next operation
	void wait_ds2483_idle();

    //Returns idle state. Returns immediatly
	byte is_ds2483_idle();

    // Perform a 1-Wire reset cycle. Returns 1 if a device responds
    // with a presence pulse.  Returns 0 if there is no device or the
    // bus is shorted or otherwise held low for more than 250uS
    byte reset(void);
    byte reset_poll(byte * presence);

	  byte readROM(void);

    // Issue a 1-Wire rom select command, you do the reset first.
    void select(const byte rom[8]);
    byte select_poll(const byte rom[8]);

    // Issue a 1-Wire rom skip command, to address all on bus.
    void skip(void);

    // Write a byte.
    void write(byte v);
    byte write_poll(byte v);

    void write_bytes(const byte *buf, uint16_t count);

    // Read a byte.
    byte read(void);
    byte read_poll(byte *result);

    void read_bytes(byte *buf, uint16_t count);

    // Write a bit.
    void write_bit(byte v);

    // Read a bit.
    byte read_bit(void);

#if DS2483_SEARCH
	  byte search_direction;

    byte triplet(byte direction);

    // Clear the search state so that if will start from the beginning again.
    void reset_search();

    // Setup the search to find the device type 'family_code' on the next call
    // to search(*newAddr) if it is present.
    void target_search(byte family_code);

    // Look for the next device. Returns 1 if a new address has been
    // returned. A zero might mean that the bus is shorted, there are
    // no devices, or you have already retrieved all of them.  It
    // might be a good idea to check the CRC to make sure you didn't
    // get garbage.  The order is deterministic. You will always get
    // the same devices in the same order.
    byte search(byte *newAddr);
#endif

#if DS2483_CRC
    // Compute a Dallas Semiconductor 8 bit CRC, these are used in the
    // ROM and scratchpad registers.
    static byte crc8(const byte *addr, byte len);

#if DS2483_CRC16
    // Compute the 1-Wire CRC16 and compare it against the received CRC.
    // Example usage (reading a DS2408):
    //    // Put everything in a buffer so we can compute the CRC easily.
    //    byte buf[13];
    //    buf[0] = 0xF0;    // Read PIO Registers
    //    buf[1] = 0x88;    // LSB address
    //    buf[2] = 0x00;    // MSB address
    //    WriteBytes(net, buf, 3);    // Write 3 cmd bytes
    //    ReadBytes(net, buf+3, 10);  // Read 6 data bytes, 2 0xFF, 2 CRC16
    //    if (!CheckCRC16(buf, 11, &buf[11])) {
    //        // Handle error.
    //    }     
    //          
    // @param input - Array of bytes to checksum.
    // @param len - How many bytes to use.
    // @param inverted_crc - The two CRC16 bytes in the received data.
    //                       This should just point into the received data,
    //                       *not* at a 16-bit integer.
    // @param crc - The crc starting value (optional)
    // @return True, iff the CRC matches.
    static bool check_crc16(const byte* input, uint16_t len, const byte* inverted_crc, uint16_t crc = 0);

    // Compute a Dallas Semiconductor 16 bit CRC.  This is required to check
    // the integrity of data received from many 1-Wire devices.  Note that the
    // CRC computed here is *not* what you'll get from the 1-Wire network,
    // for two reasons:
    //   1) The CRC is transmitted bitwise inverted.
    //   2) Depending on the endian-ness of your processor, the binary
    //      representation of the two-byte return value may have a different
    //      byte order than the two bytes you get from 1-Wire.
    // @param input - Array of bytes to checksum.
    // @param len - How many bytes to use.
    // @param crc - The crc starting value (optional)
    // @return The CRC16, as defined by Dallas Semiconductor.
    static uint16_t crc16(const byte* input, uint16_t len, uint16_t crc = 0);
#endif
#endif
};

#endif
