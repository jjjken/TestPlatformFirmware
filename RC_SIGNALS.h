#ifndef RC_SIGNALS_h
#define RC_SIGNALS_h

#include <Arduino.h> 

class RC_SIGNALS
{
public:	
	RC_SIGNALS(); // Constructor
	void init(void);
	void set(uint16_t value, byte index);
        boolean isActive(uint8_t index);
private:
        boolean ESCActive;
        boolean servo1Active;
        boolean servo2Active;
        boolean servo3Active;
};

#endif
