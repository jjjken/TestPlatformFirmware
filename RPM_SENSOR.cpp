#include <Arduino.h>
#include "RPM_SENSOR.h"

volatile unsigned long rpm_counter;
volatile unsigned long rpm_counter_time;
volatile unsigned long rpm_counter_time_old;

RPM_SENSOR::RPM_SENSOR(){}

void interrupt_counter(void);

void RPM_SENSOR::init(void){
	attachInterrupt(0, interrupt_counter, CHANGE); //interrupt will occur twice per cycle
}

float RPM_SENSOR::read_brushless_hz(){
    unsigned long rpm_pulse_time;
    float signal_hz = 0;

    //Read variables shared with interrupt
    noInterrupts();
    rpm_pulse_time = rpm_counter_time - rpm_counter_time_old;
    interrupts();

    // Conversion from microseconds to seconds, divided by two as the interrupt is
    // trigered twice per half sine, divided by 10 as we are counting 10 sines.
    if ((micros() - rpm_counter_time) >500000){
        signal_hz = 0;
    } else if (rpm_pulse_time != 0){
        signal_hz = (500000.0 * 10) / (float)rpm_pulse_time;
    }
    return signal_hz;
}

void interrupt_counter(void) {
	rpm_counter++; //Increment edge counter
    if (rpm_counter == 10) {
        rpm_counter = 0;
        rpm_counter_time_old = rpm_counter_time;
        rpm_counter_time = micros();
    }
}
