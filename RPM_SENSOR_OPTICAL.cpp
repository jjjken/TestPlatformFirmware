#include <Arduino.h>
#include "RPM_SENSOR_OPTICAL.h"
#define N_OPT_TRIG 3 // Number of triger used to estimate optical RPM speed.


static volatile uint32_t rpmTimeOptical[N_OPT_TRIG];
static volatile uint8_t rpmTimeIndex = 0;


RPM_SENSOR_OPTICAL::RPM_SENSOR_OPTICAL(){}


void RPM_SENSOR_OPTICAL::init(void){
  pinMode(4, INPUT);
  PCICR |= (1<<PCIE2); //Enable Interrupts for PCIE2 Arduino Pins (D0-7)
  PCMSK2 |= (1<<PCINT20); // Pin 4, Servo 1

  //clear the rotating filter
  int i;
  noInterrupts();
  for (i=0;i<N_OPT_TRIG;i++){
    rpmTimeOptical[i]=0;
  }
  interrupts();
  rpmTimeIndex = 0;
}

float RPM_SENSOR_OPTICAL::read_hz(){
  
  //Read variables shared with interrupt
  uint32_t newestTime;
  uint32_t oldestTime;
  noInterrupts();
  newestTime = rpmTimeOptical[rpmTimeIndex];
  oldestTime = rpmTimeOptical[ (rpmTimeIndex + N_OPT_TRIG + 1) % N_OPT_TRIG];
  interrupts();
  float rpmTime = newestTime - oldestTime;
  float signal_hz = 0;
  if ((micros()-newestTime) > 100000*N_OPT_TRIG){ // If the interrupt has not been triggered in the last 0.5s
    signal_hz = 0;
  }
  else if (rpmTime != 0) { // Prevent division by 0.
    signal_hz = 1000000.0*((float)N_OPT_TRIG-1) / (float)rpmTime;
  }
  else signal_hz = 0;  

  return signal_hz; 
}



ISR (PCINT2_vect) // handle pin change interrupt for D0 to D7 here
{
   // Optional optical RPM sensor on pin 4
   static byte lastPin4 = 0;
   byte newValue = digitalRead(4);  
   if(lastPin4 != newValue){        // Check if pin 4 was activated.
    if(newValue == 1){              // Activate only on rising edges.
        rpmTimeIndex++;
        rpmTimeIndex %= N_OPT_TRIG;
        rpmTimeOptical[rpmTimeIndex] = micros();
    }
    lastPin4 = newValue;
   }

}
