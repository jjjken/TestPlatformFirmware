#include <Arduino.h>
#include "AverageFilter.h"

AverageFilter::AverageFilter()
{
	this->counter = 0;
	this->total = 0;
	this->oldValue = 0;
}

void AverageFilter::newValue(float value)
{
	this->total += value;
	this->counter++;
	if (counter > 10000)
		getAverage();
}

float AverageFilter::getAverage()
{
	if (isData())
	{
		float value = total / counter;
		this->total = 0;
		this->counter = 0;
		this->oldValue = value;
		return value;
	}
	else
	{
		return oldValue;
	}
}

byte AverageFilter::isData()
{
	return this->counter>0;
}